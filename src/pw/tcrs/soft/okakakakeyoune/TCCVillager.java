package pw.tcrs.soft.okakakakeyoune;

import java.util.Random;

import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.village.MerchantRecipe;
import net.minecraft.village.MerchantRecipeList;
import cpw.mods.fml.common.registry.VillagerRegistry.IVillageTradeHandler;

public class TCCVillager implements IVillageTradeHandler {

	@Override
	public void manipulateTradesForVillager(EntityVillager villager,
			MerchantRecipeList recipeList, Random random) {
		/*
		 * 今回新しく作成した職業用の取引内容を登録します
		 * 既存の職業に取引を追加したりもできます
		 */
		if(villager.getProfession() == Okakakakeyoune.TCCVillagerProfession) {
			
			recipeList.add(new MerchantRecipe( new ItemStack(Item.emerald.itemID, 1, 0), Okakakakeyoune.SoySource));
			recipeList.add(new MerchantRecipe( new ItemStack(Item.emerald.itemID, 1, 0), Okakakakeyoune.sara_a));
			recipeList.add(new MerchantRecipe( new ItemStack(Item.emerald.itemID, 1, 0), new ItemStack(Okakakakeyoune.Daikon,3,0)));
			recipeList.add(new MerchantRecipe( new ItemStack(Item.emerald.itemID, 5, 0), Okakakakeyoune.Kezuriki));
			recipeList.add(new MerchantRecipe( new ItemStack(Item.emerald.itemID, 2, 0), Okakakakeyoune.Katuobusi));
			recipeList.add(new MerchantRecipe( new ItemStack(Item.emerald.itemID, 1, 0), new ItemStack(Okakakakeyoune.zip,9,0)));
			recipeList.add(new MerchantRecipe( new ItemStack(Item.emerald.itemID, 1, 0), Okakakakeyoune.nabe));
 
		}
	}
}
