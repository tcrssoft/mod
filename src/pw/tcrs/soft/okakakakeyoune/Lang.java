package pw.tcrs.soft.okakakakeyoune;

import cpw.mods.fml.common.registry.LanguageRegistry;

public class Lang {

	public static void load(Okakakakeyoune mod)
	{
		LanguageRegistry.addName(mod.bin9, "NineBottles");
		LanguageRegistry.addName(mod.Daikon, "Daikon");
		LanguageRegistry.addName(mod.DaikonN_Nabe, "Daikon no nimono pot");
		LanguageRegistry.addName(mod.daikonN_sara, "Daikon no nimono");
		LanguageRegistry.addName(mod.daikonN_sara_O, "Daikon no nimono on okaka");
		LanguageRegistry.addName(mod.k_Kezuribusi, "Roasted kezuribusi");
		LanguageRegistry.addName(mod.Katuobusi, "Katuobusi");
		LanguageRegistry.addName(mod.KatuoDasi, "Katuo dasi");
		LanguageRegistry.addName(mod.KatuoDasiNabe, "Katuo dasi pot");
		LanguageRegistry.addName(mod.Kezuribusi, "Kezuribusi");
		LanguageRegistry.addName(mod.Kezuriki, "Kezuriki");
		LanguageRegistry.addName(mod.nabe, "Pot");
		LanguageRegistry.addName(mod.Okaka, "Okaka");
		LanguageRegistry.addName(mod.sara_a, "Dish");
		LanguageRegistry.addName(mod.SoySource, "SoySource");
		LanguageRegistry.addName(mod.zip, "zip");
		LanguageRegistry.addName(mod.zip9, "Nine bags zip");
	}
	
}
