/**
 * 
 */
package pw.tcrs.soft.okakakakeyoune;

import java.util.logging.Level;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;
import net.minecraftforge.oredict.OreDictionary;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.common.registry.VillagerRegistry;
import pw.tcrs.tcrscore.TcrsCore;
import pw.tcrs.tcrscore.sum.BaseItem;
import pw.tcrs.tcrscore.sum.IMOD;

/**
 * @author TcrsSoft
 *
 * Tcrs Software License 2.0
 */
@Mod(
		modid = "Okakakakeyoune",
		name = "Okakakakeyoune",
		version = "Beta1.0",
		dependencies = "required-after:TcrsCore2.5"
		)
 
@NetworkMod(
		clientSideRequired = true,
		serverSideRequired = false
		)
public class Okakakakeyoune implements IMOD {

	protected int ITEM_ID_RANGE=30;
	public int ItemBaseID;
	public int[] ItemID = new int[ITEM_ID_RANGE];
	
	public static Item k_Kezuribusi;
	public static Item Okaka;
	public static Item SoySource;
	public static Item Kezuribusi;
	public static Item DaikonN_Nabe;
	public static Item sara_a;
	public static Item daikonN_sara;
	public static Item daikonN_sara_O;
	public static Item CakeOnOkaka;
	public static Item CakeOnOkaka_cut;
	public static Item Daikon;
	public static Item KatuoDasi;
	public static Item Kezuriki;
	public static Item_Katuobusi Katuobusi;
	public static Item zip;
	public static Item nabe;
	public static Item KatuoDasiNabe;
	public static Item bin9;
	public static Item zip9;

	public static TCCVillager villager;
	
	@SidedProxy(clientSide = "pw.tcrs.soft.okakakakeyoune.ClientProxy", serverSide = "pw.tcrs.soft.okakakakeyoune.CommonProxy")
	public static CommonProxy proxy;
	
	/*
	 * 村人の職業ID
	 * 既存の職業ID（0～4）と重複しない値を設定してください
	 */
	public static int TCCVillagerProfession = 5;
	
	public static final CreativeTabs OkakaTab = new OkakaTab("Okakakakeyoune");
	
	/* (non-Javadoc)
	 * @see pw.tcrs.tcrscore.sum.IMOD#load(cpw.mods.fml.common.event.FMLInitializationEvent)
	 */
	@Override
	@EventHandler
	public void load(FMLInitializationEvent arg0) {
		Rregistry.Load(this);
		Lang.load(this);

	}
	
	/* (non-Javadoc)
	 * @see pw.tcrs.tcrscore.sum.IMOD#preload(cpw.mods.fml.common.event.FMLPreInitializationEvent)
	 */
	@Override
	@EventHandler
	public void preload(FMLPreInitializationEvent arg0) {
		
		Configuration cfg = new Configuration(TcrsCore.getconfigfile("OkakaKakeyoune"));
		try
		{
			cfg.load();
			Property ItemStartIDProp  = cfg.getItem("ItemStart", 20000);
			ItemBaseID  = ItemStartIDProp.getInt();
		}
		catch (Exception e)
		{
			FMLLog.log(Level.SEVERE, e, "Error Message");
		}
		finally
		{
			cfg.save();
		}
		
		for(int i=0; i<ITEM_ID_RANGE; i++) {
	        ItemID[i] = ItemBaseID +i;
		}
		
		registerItem();
		
		villager = new TCCVillager();
		ResourceLocation TCCVillager=new ResourceLocation("Okakakakeyoune:");
		 
		/*
		 * 村人を登録しています(村人の職業ID,村人のテクスチャ位置)
		 */
		VillagerRegistry.instance().registerVillagerId(TCCVillagerProfession);
		 
		/*
		 * 村人の取引内容を登録しています(村人の職業ID,IVillageTradeHandlerを実装したクラス)
		 */
		VillagerRegistry.instance().registerVillageTradeHandler(TCCVillagerProfession, villager);
 
		/*
		 * 村人の家を登録しています
		 */
		VillagerRegistry.instance().registerVillageCreationHandler(new TCCVillageCreationHandleHouse());
	}

	/* (non-Javadoc)
	 * @see pw.tcrs.tcrscore.sum.IMOD#registerItem()
	 */
	@Override
	public void registerItem() {
		k_Kezuribusi= new Item(ItemID[0]).setUnlocalizedName("Okakakakeyoune:k_katuobusi").setTextureName("Okakakakeyoune:k_katuobusi")
				.setCreativeTab(OkakaTab);
		SoySource= new Item(ItemID[1]).setUnlocalizedName("Okakakakeyoune:SoySource").setTextureName("Okakakakeyoune:SoySource").setCreativeTab(OkakaTab);
		Okaka= new Item(ItemID[2]).setUnlocalizedName("Okakakakeyoune:Okaka").setTextureName("Okakakakeyoune:Okaka").setCreativeTab(OkakaTab);
		Kezuribusi= new Item(ItemID[3]).setUnlocalizedName("Okakakakeyoune:Kezuribusi").setTextureName("Okakakakeyoune:Kezuribusi").setCreativeTab(OkakaTab);
		DaikonN_Nabe= new DaikonN_Nabe(ItemID[4]).setUnlocalizedName("Okakakakeyoune:DaikonN_Nabe").setTextureName("Okakakakeyoune:Nabe").setCreativeTab(OkakaTab)
				.setMaxStackSize(1).setMaxDamage(4).setContainerItem(nabe);
		sara_a= new Item(ItemID[5]).setUnlocalizedName("Okakakakeyoune:sara_a").setTextureName("Okakakakeyoune:sara_a").setCreativeTab(OkakaTab).setMaxStackSize(8);
		
		daikonN_sara=new ItemFood(ItemID[6], 6, false){
			public ItemStack onEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
				super.onEaten(par1ItemStack, par2World, par3EntityPlayer);
				return new ItemStack(sara_a);
			}
			}.setCreativeTab(OkakaTab).setUnlocalizedName("Okakakakeyoune:daikonN_sara").setTextureName("Okakakakeyoune:daikonN_sara");
		daikonN_sara_O= new ItemFood(ItemID[7], 7, false){
			public ItemStack onEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
				super.onEaten(par1ItemStack, par2World, par3EntityPlayer);
				if (!par2World.isRemote)
				{
					par3EntityPlayer.sendChatToPlayer(ChatMessageComponent.createFromText("ますますおいしいです"));   
				}
		        return new ItemStack(sara_a);
			}}.setUnlocalizedName("Okakakakeyoune:daikonN_sara_O").setTextureName("Okakakakeyoune:daikonN_sara_O")
			.setCreativeTab(OkakaTab);
//		CakeOnOkaka=new BaseItem(ItemID[8]).setUnlocalizedName("Okakakakeyoune:CakeOnOkaka").setTextureName("Okakakakeyoune:CakeOnOkaka").setCreativeTab(OkakaTab);
//		CakeOnOkaka_cut=new ItemFood(ItemID[9], 2, false){
//			public ItemStack onEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
//				super.onEaten(par1ItemStack, par2World, par3EntityPlayer);
//		        return new ItemStack(sara_a);
//			}}.setUnlocalizedName("Okakakakeyoune:CakeOnOkaka_cut").setTextureName("Okakakakeyoune:CakeOnOkaka_cut").setCreativeTab(OkakaTab);
		
		Katuobusi = (Item_Katuobusi)(new Item_Katuobusi(ItemID[10])).setUnlocalizedName("Okakakakeyoune:Katuobusi").setTextureName("Okakakakeyoune:Katuobusi").setCreativeTab(OkakaTab).setNoRepair();
		Kezuriki = new Item_Kezuriki(ItemID[11]).setUnlocalizedName("Okakakakeyoune:Kezuriki").setTextureName("Okakakakeyoune:Kezuriki")
			.setCreativeTab(OkakaTab);
		KatuoDasi = new Item(ItemID[12]).setUnlocalizedName("Okakakakeyoune:KatuoDasi").setTextureName("Okakakakeyoune:Dasi")
				.setCreativeTab(OkakaTab);
		Daikon = new Item(ItemID[13]).setUnlocalizedName("Okakakakeyoune:Daikon").setTextureName("Okakakakeyoune:Daikon")
				.setCreativeTab(OkakaTab);
		zip = new Item(ItemID[14]).setUnlocalizedName("Okakakakeyoune:Zip").setTextureName("Okakakakeyoune:Zip")
				.setCreativeTab(OkakaTab);
		nabe = new Item(ItemID[15]).setUnlocalizedName("Okakakakeyoune:Nabe").setTextureName("Okakakakeyoune:Nabe")
				.setCreativeTab(OkakaTab);
		KatuoDasiNabe = new NabeBase(ItemID[16]).setUnlocalizedName("Okakakakeyoune:KatuoDasiNabe").setTextureName("Okakakakeyoune:Nabe").setCreativeTab(OkakaTab)
				.setMaxStackSize(1).setMaxDamage(4).setContainerItem(nabe);
		bin9 = new Item(ItemID[17]).setUnlocalizedName("Okakakakeyoune:Bin").setTextureName("Okakakakeyoune:Bin")
				.setCreativeTab(OkakaTab);
		zip9 = new Item(ItemID[18]).setUnlocalizedName("Okakakakeyoune:Zip9").setTextureName("Okakakakeyoune:Zip9")
				.setCreativeTab(OkakaTab);
		
		OreDictionary.registerOre("bottleSoySauce", SoySource);
		OreDictionary.registerOre("Okaka", Okaka);
		OreDictionary.registerOre("dashi", KatuoDasi);
		OreDictionary.registerOre("KatuoDasi", KatuoDasi);
		OreDictionary.registerOre("japaneseRadish", Daikon);
		OreDictionary.registerOre("Kezuribusi", Kezuribusi);
		
		
	}

}
