package pw.tcrs.soft.okakakakeyoune;

import net.minecraft.util.ResourceLocation;
import cpw.mods.fml.common.registry.VillagerRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ClientProxy extends CommonProxy{
	
	public static void preload(Okakakakeyoune mod)
	{
		VillagerRegistry.instance().registerVillagerSkin(mod.TCCVillagerProfession, new ResourceLocation("okakakakeyoune", "textures/mob/tccv1.png"));
	}

}
