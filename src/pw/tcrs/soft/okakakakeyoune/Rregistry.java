package pw.tcrs.soft.okakakakeyoune;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import cpw.mods.fml.common.registry.GameRegistry;

public class Rregistry {
	
	public static void Load(Okakakakeyoune mod) {
		
		//鰹節を削る
		GameRegistry.addShapelessRecipe(new ItemStack(mod.Kezuribusi),
				new ItemStack(mod.Katuobusi,1,32767), new ItemStack(mod.Kezuriki), new ItemStack(mod.zip));
		
		//煎るよ！
		GameRegistry.addSmelting(mod.Kezuribusi.itemID, new ItemStack(mod.k_Kezuribusi), 1);
				
		//ｵｶｶﾂｸﾛｳﾈｰ
		GameRegistry.addShapelessRecipe(new ItemStack(mod.Okaka, 8), new Object[]{
			new ItemStack(mod.k_Kezuribusi),new ItemStack(mod.k_Kezuribusi),new ItemStack(mod.k_Kezuribusi),
			new ItemStack(mod.k_Kezuribusi),new ItemStack(mod.k_Kezuribusi),new ItemStack(mod.k_Kezuribusi),
			new ItemStack(mod.k_Kezuribusi),new ItemStack(mod.SoySource),new ItemStack(Item.sugar)
		});
		//鉱石辞書対応ｼﾖｳﾈｰ
		GameRegistry.addRecipe(
				new ShapelessOreRecipe(
					new ItemStack(mod.Okaka, 8),
						new Object[]
						{
							"bottleSoySauce", mod.k_Kezuribusi,mod.k_Kezuribusi,mod.k_Kezuribusi,mod.k_Kezuribusi,
							mod.k_Kezuribusi,mod.k_Kezuribusi,mod.k_Kezuribusi,Item.sugar
						}));
		
		//大根の煮物(加熱前)
		GameRegistry.addShapelessRecipe(new ItemStack(mod.DaikonN_Nabe, 1, 4), new Object[]{
			new ItemStack(mod.nabe),mod.Daikon,mod.KatuoDasi,mod.SoySource
		});
		
		//鉱石辞書対応ｼﾖｳﾈｰ
				GameRegistry.addRecipe(
						new ShapelessOreRecipe(
							new ItemStack(mod.DaikonN_Nabe, 1, 4),
								new Object[]
								{
									"bottleSoySauce", "KatuoDasi","japaneseRadish",mod.nabe
								}));
		
		//大根の煮物を調理するよ！
		FurnaceRecipes.smelting().addSmelting(mod.DaikonN_Nabe.itemID, 4, new ItemStack(mod.DaikonN_Nabe,1,3),1);
		FurnaceRecipes.smelting().addSmelting(mod.DaikonN_Nabe.itemID, 3, new ItemStack(mod.DaikonN_Nabe,1,2),1);
		FurnaceRecipes.smelting().addSmelting(mod.DaikonN_Nabe.itemID, 2, new ItemStack(mod.DaikonN_Nabe,1,1),1);
		FurnaceRecipes.smelting().addSmelting(mod.DaikonN_Nabe.itemID, 1, new ItemStack(mod.DaikonN_Nabe,1,0),1);
		
		//それは桜乃さんの大根をアンジェが煮たものでございます。
		GameRegistry.addShapelessRecipe(new ItemStack(mod.daikonN_sara, 5), new Object[]{
			new ItemStack(mod.sara_a),new ItemStack(mod.sara_a),new ItemStack(mod.sara_a),
			new ItemStack(mod.sara_a),new ItemStack(mod.sara_a),new ItemStack(mod.DaikonN_Nabe, 1, 0)
		});
		
		//ｵｶｶｶｹﾖｳﾈｰ
		GameRegistry.addShapelessRecipe(new ItemStack(mod.daikonN_sara_O), mod.daikonN_sara,mod.Okaka);
		//鉱石辞書対応ｼﾖｳﾈｰ
		GameRegistry.addRecipe(
				new ShapelessOreRecipe(
					new ItemStack(mod.daikonN_sara_O),
						new Object[]
						{
							"Okaka",mod.daikonN_sara
						}));
		
		//だしを取るよ(加熱前)
		GameRegistry.addShapelessRecipe(new ItemStack(mod.KatuoDasiNabe, 1, 4), new Object[]{
			new ItemStack(mod.nabe),mod.Kezuribusi,mod.Kezuribusi,mod.Kezuribusi,mod.SoySource,Item.bucketWater
		});
		//鉱石辞書対応ｼﾖｳﾈｰ
		GameRegistry.addRecipe(
				new ShapelessOreRecipe(
					new ItemStack(mod.KatuoDasiNabe, 1, 4),
						new Object[]
						{
							"bottleSoySauce", "Kezuribusi","Kezuribusi","Kezuribusi",mod.nabe,Item.bucketWater
						}));
		
		//だしを取るよ
		FurnaceRecipes.smelting().addSmelting(mod.KatuoDasiNabe.itemID, 4, new ItemStack(mod.KatuoDasiNabe,1,3),1);
		FurnaceRecipes.smelting().addSmelting(mod.KatuoDasiNabe.itemID, 3, new ItemStack(mod.KatuoDasiNabe,1,2),1);
		FurnaceRecipes.smelting().addSmelting(mod.KatuoDasiNabe.itemID, 2, new ItemStack(mod.KatuoDasiNabe,1,1),1);
		FurnaceRecipes.smelting().addSmelting(mod.KatuoDasiNabe.itemID, 1, new ItemStack(mod.KatuoDasiNabe,1,0),1);
		
		GameRegistry.addShapelessRecipe(new ItemStack(mod.KatuoDasi, 27), new Object[]{
			new ItemStack(mod.bin9),new ItemStack(mod.bin9),new ItemStack(mod.bin9),new ItemStack(mod.KatuoDasiNabe, 1, 0)
		});
		
		//瓶の塊
		GameRegistry.addShapelessRecipe(new ItemStack(mod.bin9),new Object[]{new ItemStack(Item.glassBottle),Item.glassBottle,Item.glassBottle
				,Item.glassBottle,Item.glassBottle,Item.glassBottle,Item.glassBottle,Item.glassBottle,Item.glassBottle});
		//上の逆
		GameRegistry.addShapelessRecipe(new ItemStack(Item.glassBottle,9), mod.bin9);
		
	}

}
