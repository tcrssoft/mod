package pw.tcrs.soft.okakakakeyoune;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;

public class OkakaTab extends CreativeTabs{

	public OkakaTab(String label){
		super(label);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getTabIconItemIndex()
	{
		return Okakakakeyoune.Okaka.itemID;
	}
 
	@Override
	@SideOnly(Side.CLIENT)
	public String getTranslatedTabLabel()
	{
		return "Okakakakeyoune";
	}
	
}
